# Description

Installs and configures Webgrind.

Important: The default vHost configuration requires an available ssl certificate that is created e.g. by the
typo3_site cookbook.

# Usage

This will allow you to run the profiler on a request by adding the following params to a request `?XDEBUG_PROFILE`.
