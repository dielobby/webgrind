name 'webgrind'
maintainer 'Paul Ilea'
maintainer_email 'p.ilea@die-lobby.de'
license 'Apache 2.0'
description 'Webgrind'
long_description IO.read(File.join(File.dirname(__FILE__), 'README.md'))
version '1.0.0'
issues_url 'https://bitbucket.org/dielobby/webgrind/issues'
source_url 'https://bitbucket.org/dielobby/webgrind'

recipe 'webgrind::default', 'Installs Webgrind'
recipe 'webgrind::webgrind', 'Webgrind'

depends :apache2
depends :database
depends :git
depends :hostsfile
