#
# Cookbook Name:: webgrind
# Recipe:: webgrind
#
# Copyright Paul Ilea
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the 'Software'), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.

############################
### Install Dependencies ###
############################

package 'graphviz'

webgrind_ip_address = node['webgrind']['ip_address'] || node['ubuntu_base']['ip_address']
base_hostname = node['webgrind']['base_hostname'] || node['ubuntu_base']['hostname']
webgrind_hostname = node['webgrind']['hostname'] || "webgrind.#{base_hostname}"

#######################
### Download XhProf ###
#######################

directory node['webgrind']['install_path'] do
	recursive true
	action :create
end

git node['webgrind']['install_path'] do
	repository 'git://github.com/jokkedk/webgrind.git'
	revision 'master'
	action :sync
end

###########################
### Setup Configuration ###
###########################

template node['webgrind']['install_path'] + '/config.php' do
	source 'webgrind.config.php.erb'
	mode '0644'
	variables(
		:params => node['webgrind']
	)
	action :create
end

##########################
### Setup Virtual Host ###
##########################

# ssl certificate needed (requires that typo3_site (or a kind of) was executed before this script)
web_app webgrind_hostname do
	server_name webgrind_hostname
	apache node['apache']
	docroot node['webgrind']['install_path'] + '/'
end

template '/etc/apache2/sites-available/' + webgrind_hostname + '.conf' do
	source 'web_app.conf.erb'
	mode '0644'
	variables(
		:params => {
			:install_path => node['webgrind']['install_path'],
			:hostname => webgrind_hostname,
			:base_hostname => base_hostname
		}
	)
	action :create
end

service 'apache2' do
	action :restart
end
