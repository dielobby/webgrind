#
# Cookbook Name:: webgrind
# Recipe:: default
#
# Copyright Paul Ilea
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.

webgrind_ip_address = node['webgrind']['ip_address'] || node['ubuntu_base']['ip_address']
webgrind_hostname = node['webgrind']['hostname'] || "webgrind.#{node['ubuntu_base']['hostname']}"

if node['platform_version'] == '16.04'
	include_recipe 'webgrind::webgrind'

	hostsfile_entry webgrind_ip_address do
		hostname webgrind_hostname
		action :append
	end
else
	log 'webgrind not supported' do
		message 'Webgrind is not supported in combination with Ubuntu 14.04'
		level :warn
	end
end


